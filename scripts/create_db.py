""" This is just test for creation of database"""

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db_demo.sqlite'
db = SQLAlchemy(app)


class Strain(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    strain_name = db.Column(db.String(200))
    desc = db.Column(db.String(200))

    def __init__(self, strain_name, desc):
        self.strain_name = strain_name
        self.desc = desc

    def __repr__(self):
        return '<Strain : %r>' % self.strain_name


class Sample(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    sample_name = db.Column(db.String(200))
    desc = db.Column(db.String(200))

    strain_id = db.Column(db.Integer, db.ForeignKey('strain.id'))
    strain = db.relationship('strain', backref=db.backref('strains', lazy='dynamic'))

    def __init__(self, sample_name, desc, strain):
        self.sample_name = sample_name
        self.desc = desc
        self.strain = strain

    def __repr__(self):
        return '<Sample %r>' % self.sample_name

db.create_all()