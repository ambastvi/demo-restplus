import logging

from flask import request
from flask_restplus import Resource

from demo_api.api.demo.business_demo import SampleBusiness
from demo_api.api.demo.database.demo_model import Sample
from demo_api.api.demo.demo_serializers import sample, page_of_demo_samples
from demo_api.api.demo.parsers import pagination_arguments
from demo_api.api.restplus import api

log = logging.getLogger(__name__)

demo_ns = api.namespace('demo/samples', description='Operations related to demo samples')


@demo_ns.route('/')
class SamplesCollection(Resource):

    @api.expect(pagination_arguments)
    @api.marshal_with(page_of_demo_samples)
    def get(self):
        """
        Returns list of demp samples.
        """
        args = pagination_arguments.parse_args(request)
        page = args.get('page', 1)
        per_page = args.get('per_page', 10)

        samples_query = Sample.query
        samples_page = samples_query.paginate(page, per_page, error_out=False)

        return samples_page

    @api.expect(sample)
    def post(self):
        """
        Creates a new demp sample.
        """
        SampleBusiness.create_sample(request.json)
        return "success", 201


@demo_ns.route('/<int:id>')
@api.response(404, 'Sample not found.')
class PostItem(Resource):

    @api.marshal_with(sample)
    def get(self, id):
        """
        Returns a demo sample.
        """
        return Sample.query.filter(Sample.id == id).one()

    @api.expect(sample)
    @api.response(204, 'Sample successfully updated.')
    def put(self, id):
        """
        Update a blog post.
        """
        data = request.json
        SampleBusiness.update_sample(id, data)
        return "success", 204

    @api.response(204, 'Sample successfully deleted.')
    def delete(self, id):
        """
        Delete demo post.
        """
        SampleBusiness.delete_sample(id)
        return None, 204


@demo_ns.route('/archive/<int:id>/')
class PostsArchiveCollection(Resource):

    @api.expect(pagination_arguments, validate=True)
    @api.marshal_with(page_of_demo_samples)
    def get(self, id):
        """
        Returns list of demp samples greated than given id value.
        """
        args = pagination_arguments.parse_args(request)
        page = args.get('page', 1)
        per_page = args.get('per_page', 10)

        samples_query = Sample.query.filter(Sample.id > id)
        sample_page = samples_query.paginate(page, per_page, error_out=False)

        return sample_page