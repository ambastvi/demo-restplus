import logging

from flask import request
from flask_restplus import Resource

from demo_api.api.demo.business_demo import StrainBusiness
from demo_api.api.demo.database.demo_model import Strain
from demo_api.api.demo.demo_serializers import strain, strain_with_samples
from demo_api.api.restplus import api

log = logging.getLogger(__name__)
demo_ns = api.namespace('demo/strains', description='Operations related to demo strains')


@demo_ns.route('/')
class strainCollection(Resource):

    @api.marshal_list_with(strain)
    def get(self):
        """
        Returns list of demo strains.
        """
        strains = Strain.query.all()
        return strains

    @api.response(201, 'Strain successfully created.')
    @api.expect(strain)
    def post(self):
        """
        Creates a new demo strain.
        """
        data = request.json
        StrainBusiness.create_strain(data)
        return "success", 201


@demo_ns.route('/<int:id>')
@api.response(404, 'Strain not found.')
class StrainItem(Resource):

    @api.marshal_with(strain_with_samples)
    def get(self, id):
        """
        Returns a strain with a list of samples.
        """
        return Strain.query.filter(Strain.id == id).one()

    @api.expect(strain)
    @api.response(204, 'Category successfully updated.')
    def put(self, id):
        """
        Updates a demo strain.

        Use this method to change the name of a demo strain.

        * Send a JSON object with the new name in the request body.

        ```
        {
          "strain_name": "New strain Name",
          "desc": "Description"

        }
        ```

        * Specify the ID of the strain to modify in the request URL path.
        """
        data = request.json
        StrainBusiness.update_strain(id, data)
        return None, 204

    @api.response(204, 'Strain successfully deleted.')
    def delete(self, id):
        """
        Deletes demp strain.
        """
        StrainBusiness.delete_strain(id)
        return None, 204
