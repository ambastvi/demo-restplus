# The examples in this file come from the Flask-SQLAlchemy documentation
# For more information take a look at:
# http://flask-sqlalchemy.pocoo.org/2.1/quickstart/#simple-relationships

from demo_api.api.demo.database import db


class Strain(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    strain_name = db.Column(db.String(200))
    desc = db.Column(db.String(200))

    def __init__(self, id, strain_name, desc):
        self.id = id
        self.strain_name = strain_name
        self.desc = desc

    def __repr__(self):
        return '<Strain : %r>' % self.strain_name


class Sample(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    sample_name = db.Column(db.String(200))
    desc = db.Column(db.String(200))

    strain_id = db.Column(db.Integer, db.ForeignKey('strain.id'))
    strain = db.relationship('Strain', backref=db.backref('strains', lazy='dynamic'))

    def __init__(self, sample_name, desc, strain):
        self.sample_name = sample_name
        self.desc = desc
        self.strain = strain

    def __repr__(self):
        return '<Sample %r>' % self.sample_name
