from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


def reset_database():
    from demo_api.api.demo.database.demo_model import Sample, Strain
    db.drop_all()
    db.create_all()


