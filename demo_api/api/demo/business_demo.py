from demo_api.api.demo.database import db

from demo_api.api.demo.database.demo_model import Strain, Sample


class StrainBusiness():

    @staticmethod
    def create_strain(data):
        strain_id = data.get('id')
        strain_name = data.get('strain_name')
        desc = data.get('desc')

        try:
            strainObj = Strain(strain_id, strain_name, desc)
        except Exception as e:
            return e.__str__()
        db.session.add(strainObj)
        db.session.commit()

    @staticmethod
    def update_strain(strain_id, data):
        strain = Strain.query.filter(Strain.id == strain_id).one()

        strain_name = data.get('strain_name', None)
        if strain_name:
            strain.strain_name = strain_name

        desc = data.get('desc', None)
        if desc:
            strain.desc = desc

        db.session.add(strain)
        db.session.commit()

    @staticmethod
    def delete_strain(strain_id):
        strain = Strain.query.filter(Strain.id == strain_id).one()
        if strain:
            db.session.delete(strain)
            db.session.commit()


class SampleBusiness():

    @staticmethod
    def create_sample(data):
        sample_name = data.get('sample_name')
        desc = data.get('desc')
        strain_id = data.get('strain_id')

        strain = Strain.query.filter(Strain.id == strain_id).one()
        sample = Sample(sample_name, desc, strain)
        db.session.add(sample)
        db.session.commit()

    @staticmethod
    def update_sample(sample_id, data):
        sample = Sample.query.filter(Sample.id == sample_id).one()

        sample_name = data.get('sample_name', None)
        if sample_name:
            sample.desc = data.get('sample_name')

        desc = data.get('desc', None)
        if desc:
            sample.desc = desc

        strain_id = data.get('strain_id', None)
        if strain_id:
            sample.strain = Strain.query.filter(Strain.id == strain_id).one()

        db.session(sample)
        db.session.commit()

    def delete_sample(sample_id):
        sample = Sample.query.filter(Sample.id == sample_id).one()
        db.session.delete(sample)
        db.session.commit()