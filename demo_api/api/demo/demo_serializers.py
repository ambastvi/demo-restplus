from flask_restplus import fields
from demo_api.api.restplus import api

strain = api.model('strain',{
    'id' : fields.Integer(readOnly = True, description = 'The unique identifier of a demo strain'),
    'strain_name': fields.String(required=True, description='Name of strain'),
    'desc': fields.String(required=True, description='Description about strain')
})

sample = api.model('sample',{
    'id' : fields.Integer(readOnly = True, description = 'The unique identifier of a demo sample'),
    'sample_name': fields.String(required=True, description='Name of sample'),
    'desc': fields.String(required=True, description='Description about sample'),
    'strain_id': fields.Integer(attribute='strain.id'),
    'strain': fields.String(attribute='strain.id')
})

pagination = api.model('A page of results', {
    'page': fields.Integer(description='Number of this page of results'),
    'pages': fields.Integer(description='Total number of pages of results'),
    'per_page': fields.Integer(description='Number of items per page of results'),
    'total': fields.Integer(description='Total number of results'),
})

page_of_demo_samples = api.inherit('Page of Samples', pagination, {
    'items': fields.List(fields.Nested(sample))
})


strain_with_samples = api.inherit('Demo strain with samples', strain, {
    'strains': fields.List(fields.Nested(sample))
})
